--liquibase formatted sql

--changeset rajeev.s:1552476825312-1
CREATE SEQUENCE hibernate_sequence;

--changeset rajeev.s:1552476825312-2
CREATE SEQUENCE teachers_sequence;

--changeset rajeev.s:1552476825312-3
CREATE TABLE teachers (
  id            BIGINT                                 NOT NULL,
  age           INTEGER                                NOT NULL,
  city          TEXT                                   NOT NULL,
  creation_time TIMESTAMP with time zone DEFAULT NOW() NOT NULL,
  teacher_name  TEXT                                   NOT NULL,
  CONSTRAINT "teachersPK" PRIMARY KEY (id)
);

--changeset rajeev.s:1552476825312-4
CREATE TABLE users (
  id   BIGSERIAL NOT NULL,
  age  INTEGER   NOT NULL,
  city TEXT      NOT NULL,
  name TEXT      NOT NULL,
  CONSTRAINT "usersPK" PRIMARY KEY (id)
);

