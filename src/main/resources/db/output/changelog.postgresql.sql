-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/resources/db/changelog-master.xml
-- Ran at: 2/7/19, 3:29 PM
-- Against: rajeev.s@jdbc:postgresql://localhost:5432/learn_hibernate
-- Liquibase version: 3.6.3
-- *********************************************************************

-- Create Database Lock Table
CREATE TABLE databasechangeloglock (ID INTEGER NOT NULL, LOCKED BOOLEAN NOT NULL, LOCKGRANTED TIMESTAMP WITHOUT TIME ZONE, LOCKEDBY VARCHAR(255), CONSTRAINT DATABASECHANGELOGLOCK_PKEY PRIMARY KEY (ID));

-- Initialize Database Lock Table
DELETE FROM databasechangeloglock;

INSERT INTO databasechangeloglock (ID, LOCKED) VALUES (1, FALSE);

-- Lock Database
UPDATE databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = '172.27.170.64 (172.27.170.64)', LOCKGRANTED = '2019-02-07 15:29:41.624' WHERE ID = 1 AND LOCKED = FALSE;

-- Create Database Change Log Table
CREATE TABLE databasechangelog (ID VARCHAR(255) NOT NULL, AUTHOR VARCHAR(255) NOT NULL, FILENAME VARCHAR(255) NOT NULL, DATEEXECUTED TIMESTAMP WITHOUT TIME ZONE NOT NULL, ORDEREXECUTED INTEGER NOT NULL, EXECTYPE VARCHAR(10) NOT NULL, MD5SUM VARCHAR(35), DESCRIPTION VARCHAR(255), COMMENTS VARCHAR(255), TAG VARCHAR(255), LIQUIBASE VARCHAR(20), CONTEXTS VARCHAR(255), LABELS VARCHAR(255), DEPLOYMENT_ID VARCHAR(10));

-- Changeset src/main/resources/db/changelog/create-users-table.sql::raw::includeAll
CREATE TABLE users (
        id SERIAL NOT NULL,
        name TEXT NOT NULL,
        city TEXT NOT NULL,
        age INTEGER NOT NULL);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('raw', 'includeAll', 'src/main/resources/db/changelog/create-users-table.sql', NOW(), 1, '8:bc59f8de8dfc0d26d32b72de3fed49cd', 'sql', '', 'EXECUTED', NULL, NULL, '3.6.3', '9533583480');

-- Changeset src/main/resources/db/changelog/insert-users-table.sql::raw::includeAll
INSERT INTO users (name,city,age) VALUES ('Kirit.p','Mumbai',37),
        ('Manish.m','Mumbai',40),
        ('Supriya.m','Mumbai',37),
        ('Rahul.go','Mumbai',30);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('raw', 'includeAll', 'src/main/resources/db/changelog/insert-users-table.sql', NOW(), 2, '8:62e3be97facf1a838fcb8db9c423565d', 'sql', '', 'EXECUTED', NULL, NULL, '3.6.3', '9533583480');

-- Changeset src/main/resources/db/changelog/add-column-gender.sql::raw::includeAll
ALTER TABLE users ADD gender TEXT NOT NULL DEFAULT 'MALE';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('raw', 'includeAll', 'src/main/resources/db/changelog/add-column-gender.sql', NOW(), 3, '8:d7cf934dbafd7fc5d2eba6bfbe511eb3', 'sql', '', 'EXECUTED', NULL, NULL, '3.6.3', '9533583480');

-- Changeset src/main/resources/db/changelog/insert-more-users.sql::raw::includeAll
INSERT INTO users (name,city,age) VALUES ('gopal.r','Mumbai',40);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('raw', 'includeAll', 'src/main/resources/db/changelog/insert-more-users.sql', NOW(), 4, '8:01976514ec88c86d85bed1cf3e331b94', 'sql', '', 'EXECUTED', NULL, NULL, '3.6.3', '9533583480');

-- Changeset src/main/resources/db/changelog/insert-more-users2.sql::raw::includeAll
INSERT INTO users (name,city,age) VALUES ('birju.s','Mumbai',35);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('raw', 'includeAll', 'src/main/resources/db/changelog/insert-more-users2.sql', NOW(), 5, '8:7fbb2b8a3c4cafe3f7ba9ea771b7addf', 'sql', '', 'EXECUTED', NULL, NULL, '3.6.3', '9533583480');

-- Release Database Lock
UPDATE databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

