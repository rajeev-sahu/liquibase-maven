package com.db.version.maven.dbversionmaven.entities;

import javax.persistence.*;

@Entity
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false, columnDefinition = " TEXT")
    private String name;

    @Column(name = "city", nullable = false, columnDefinition = " TEXT")
    private String city;

    @Column(name = "age", nullable = false, columnDefinition = " INTEGER")
    private Integer age;


}
