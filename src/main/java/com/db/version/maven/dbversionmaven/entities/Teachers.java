package com.db.version.maven.dbversionmaven.entities;


import javax.persistence.*;
import java.time.Instant;

@Entity
public class Teachers {

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "teachers_generator", sequenceName = "teachers_sequence",initialValue = 1, allocationSize = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teachers_generator")
    private Long id;


    @Column(name = "teacher_name", nullable = false, columnDefinition = " TEXT")
    private String name;

    @Column(name = "city", nullable = false, columnDefinition = " TEXT")
    private String city;

    @Column(name = "age", nullable = false, columnDefinition = " INTEGER")
    private Integer age;

    @Column(name = "creation_time", columnDefinition = " timestamp with time zone DEFAULT now()" ,nullable = false)
    private Instant creation_time;


}
