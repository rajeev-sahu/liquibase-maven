package com.db.version.maven.dbversionmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbVersionMavenApplication {

    public static void main(String[] args) {

        SpringApplication.run(DbVersionMavenApplication.class, args);
    }

}

